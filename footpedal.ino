const int btnPin = PIN_D0;
const int ledPin = PIN_D3;

int btnPushCount = 0;   // number of button presses
int btnState = 0;         // current button state


void setup()
{
  pinMode(btnPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  Serial.println(digitalRead(btnPin));
  if (!digitalRead(btnPin)) {   // active low circuit means that a released button registers as `1`
    Keyboard.press(KEY_F13);
  } else {
    Keyboard.release(KEY_F13);
  }

  delay(50);  // debounce
}
